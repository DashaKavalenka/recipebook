import {Component, OnInit} from '@angular/core';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'PROJECT';
  loadedFeature = 'recipe';
  ngOnInit() {
    firebase.initializeApp({
      apiKey: 'AIzaSyDtQ7SWy3gMVRDxbYDEaRgyB2gOnlz4Jd8',
      authDomain: 'ng-recipe-cookbook.firebaseapp.com'
    });

  }
  onNavigate(feature: string) {
    this.loadedFeature = feature;
  }
}
