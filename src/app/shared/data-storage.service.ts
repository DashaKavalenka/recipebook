import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams, HttpRequest} from '@angular/common/http';
import { map } from 'rxjs/operators';

import {RecipeService} from '../recipes/recipe.service';
import {Recipe} from '../recipes/recipe.modal';
import {AuthService} from '../auth/auth.service';

@Injectable()
export class DataStorageService {
  constructor(private httpClient: HttpClient,
              private resipeService: RecipeService,
              private authService: AuthService) {
  }

  storeRecipe() {
    // const token = this.authService.getToken();
    // return this.httpClient.put('https://ng-recipe-cookbook.firebaseio.com/recipes.json',
    //   this.resipeService.getRecipes(), {
    //     observe: 'body',
    //     params: new HttpParams().set('auth', token)
    //   // headers: new HttpHeaders().set('Auth', 'Bearer ghghgjgjhghjg')
    //     });
    const req = new HttpRequest('PUT', 'https://ng-recipe-cookbook.firebaseio.com/recipes.json',
      this.resipeService.getRecipes(), {reportProgress: true});
    return this.httpClient.request(req);
  }

  getRecipes() {
    // const token = this.authService.getToken();

    // this.httpClient.get<Recipe[]>('https://ng-recipe-cookbook.firebaseio.com/recipes.json?auth=' + token)
    this.httpClient.get<Recipe[]>('https://ng-recipe-cookbook.firebaseio.com/recipes.json', {
      observe: 'body',
      responseType: 'json'
    })
      .pipe(map(
        (recipes) => {
          console.log(recipes);
          for (const recipe of recipes) {
            if (!recipe['ingredients']) {
              recipe['ingredients'] = [];
            }
          }
          return recipes;
        }
      ))
      .subscribe(
        (recipes: Recipe[]) => {
          this.resipeService.setRecipes(recipes);
        }
      );
  }
}
