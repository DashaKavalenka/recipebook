import {Subject} from 'rxjs';

import {Recipe} from './recipe.modal';
import {Ingredient} from '../shared/ingredient.model';

export class RecipeService {
  recipesChanged = new Subject<Recipe[]>();

  private recipes: Recipe[] = [
    new Recipe(
      'A test name',
      'Spagetti',
      'https://i.etsystatic.com/6604404/r/il/ec3b26/1294701570/il_570xN.1294701570_iryo.jpg',
      [
        new Ingredient('Meat', 1),
        new Ingredient('Tomato', 20)
      ]),
    new Recipe(
      'Name test',
      'Appetizer',
      'https://i.etsystatic.com/6604404/r/il/ec3b26/1294701570/il_570xN.1294701570_iryo.jpg',
      [
        new Ingredient('Wine', 1),
        new Ingredient('Cheese', 20)
      ])
  ];

  constructor() {
  }

  setRecipes(recipes: Recipe[]) {
    this.recipes = recipes;
    this.recipesChanged.next(this.recipes.slice());
  }

  getRecipes() {
    return this.recipes.slice();
  }

  getRecipe(index: number) {
    return this.recipes[index];
  }
    addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.recipesChanged.next(this.recipes.slice());
  }

  updateRecipe(index: number, newRecipe: Recipe) {
    this.recipes[index] = newRecipe;
    this.recipesChanged.next(this.recipes.slice());
  }

  delRecipe(index: number) {
    this.recipes.splice(index, 1);
    this.recipesChanged.next(this.recipes.slice());
  }
}
